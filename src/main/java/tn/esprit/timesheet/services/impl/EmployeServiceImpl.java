package tn.esprit.timesheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import tn.esprit.timesheet.entities.*;
import tn.esprit.timesheet.reposetoris.IContratRepository;
import tn.esprit.timesheet.reposetoris.IDepartementsRepository;
import tn.esprit.timesheet.reposetoris.IEmployesRepository;
import tn.esprit.timesheet.services.interfaces.EmployeService;
import java.util.List;


import org.apache.log4j.Logger;

@Service
public class EmployeServiceImpl implements EmployeService {

	private static final Logger logger = Logger.getLogger(EmployeServiceImpl.class);
	@Autowired
	IEmployesRepository employesRepository;
	@Autowired
	IDepartementsRepository departementsRepository;
	@Autowired
	IContratRepository contratRepository;
	@Override
	public int ajouterEmploye(Employe employe) {
		return employesRepository.save(employe).getId();
	}

	@Override
	public void affecterEmployeADepartement(int employeId, int depId) {
		Employe employe = employesRepository.findById((long) employeId).get();
		Departement departement = departementsRepository.findById((long) depId).get();
		List<Departement> lDepartement = employe.getDepartements();
		lDepartement.add(departement);
		employe.setDepartements(lDepartement);
		employesRepository.save(employe);
	}

	@Override
	public int ajouterContrat(Contrat contrat) {
		return contratRepository.save(contrat).getReference();
	}

	@Override
	public void affecterContratAEmploye(int contratId, int employeId) {
		Employe employe = employesRepository.findById((long) employeId).get();
		Contrat contrat = contratRepository.findById((long) contratId).get();
		employe.setContrat(contrat);
		employesRepository.save(employe);
	}

	@Override
	public String getEmployePrenomById(int employeId) {
		return employesRepository.findById((long) employeId).get().getPrenom();
	}

	@Override
	public long getNombreEmployeJPQL() {
		return employesRepository.getNombreEmploye();
	}

	@Override
	public List<String> getAllEmployeNamesJPQL() {
		return employesRepository.getAllEmployeNames();
	}
}
