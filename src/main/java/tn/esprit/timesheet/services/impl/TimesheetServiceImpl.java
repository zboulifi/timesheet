package tn.esprit.timesheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.timesheet.entities.*;
import tn.esprit.timesheet.reposetoris.IDepartementsRepository;
import tn.esprit.timesheet.reposetoris.IMissionRepository;
import tn.esprit.timesheet.reposetoris.ITimesheetRepository;
import tn.esprit.timesheet.services.interfaces.TimesheetService;

import java.util.Date;


@Service
public class TimesheetServiceImpl implements TimesheetService {

    @Autowired
    IMissionRepository missionRepository;
    @Autowired
    IDepartementsRepository departementsRepository;
    @Autowired
    ITimesheetRepository timesheetRepository;

    @Override
    public int ajouterMission(Mission mission) {
        return missionRepository.save(mission).getId();
    }

    @Override
    public void affecterMissionADepartement(int missionId, int depId) {
        Mission mission = missionRepository.findById((long) missionId).get();
        Departement dep = departementsRepository.findById((long) depId).get();
        mission.setDepartement(dep);
        missionRepository.save(mission);
    }

    @Override
    public void ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin) {
        TimesheetPK timesheetPK = new TimesheetPK();
        timesheetPK.setDateDebut(dateDebut);
        timesheetPK.setDateFin(dateFin);
        timesheetPK.setIdEmploye(employeId);
        timesheetPK.setIdMission(missionId);

        Timesheet timesheet = new Timesheet();
        timesheet.setTimesheetPK(timesheetPK);
        timesheet.setValide(false);
        timesheetRepository.save(timesheet);
    }

}
