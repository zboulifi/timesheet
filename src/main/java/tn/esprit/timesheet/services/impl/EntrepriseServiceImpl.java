package tn.esprit.timesheet.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.timesheet.entities.Departement;
import tn.esprit.timesheet.entities.Entreprise;
import tn.esprit.timesheet.reposetoris.IDepartementsRepository;
import tn.esprit.timesheet.reposetoris.IEntrepriseRepository;
import tn.esprit.timesheet.services.interfaces.EntrepriseService;

import java.util.ArrayList;
import java.util.List;


@Service
public class EntrepriseServiceImpl implements EntrepriseService {
    @Autowired
    IEntrepriseRepository entrepriseRepository;
    @Autowired
    IDepartementsRepository departementsRepository;

    @Override
    public int ajouterEntreprise(Entreprise entreprise) {
        return entrepriseRepository.save(entreprise).getId();
    }

    @Override
    public int ajouterDepartement(Departement dep) {
        return departementsRepository.save(dep).getId();
    }

    @Override
    public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
        Entreprise entreprise = entrepriseRepository.findById((long) entrepriseId).get();
        Departement departement = departementsRepository.findById((long) depId).get();
        List<Departement> lDepartement = entreprise.getDepartements();
        lDepartement.add(departement);
        entreprise.setDepartements(lDepartement);
        entrepriseRepository.save(entreprise);
    }

    @Override
    public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {

        Entreprise entreprise = entrepriseRepository.findById((long) entrepriseId).get();
        List<String> depNames = new ArrayList<>();
        //List<String> depNames = (List<String>) entreprise.getDepartements().stream().map(departement -> departement.getName());

         for( Departement departement : entreprise.getDepartements()){
            depNames.add(departement.getName());
        }
        return depNames;
    }
}
