package tn.esprit.timesheet.reposetoris;

import org.springframework.data.repository.CrudRepository;
import tn.esprit.timesheet.entities.Mission;

public interface IMissionRepository extends CrudRepository<Mission,Long> {
}
