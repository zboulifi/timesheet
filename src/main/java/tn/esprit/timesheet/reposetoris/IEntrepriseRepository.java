package tn.esprit.timesheet.reposetoris;

import org.springframework.data.repository.CrudRepository;
import tn.esprit.timesheet.entities.Entreprise;

import java.util.List;

public interface IEntrepriseRepository extends CrudRepository<Entreprise,Long> {
    List<String> findByPrenom(String prenom);
}
