package tn.esprit.timesheet.reposetoris;

import org.springframework.data.repository.CrudRepository;
import tn.esprit.timesheet.entities.Departement;

public interface IDepartementsRepository extends CrudRepository<Departement,Long> {
}
