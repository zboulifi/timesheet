package tn.esprit.timesheet.reposetoris;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.timesheet.entities.Employe;

import java.util.List;

@Repository
public interface IEmployesRepository extends CrudRepository<Employe,Long> {
    //Employe findByPrenom(String prenom);
    @Query("select Count(e) from Employe e")
    long getNombreEmploye();
    @Query("select e.nom from Employe e")
    List<String> getAllEmployeNames();
}
