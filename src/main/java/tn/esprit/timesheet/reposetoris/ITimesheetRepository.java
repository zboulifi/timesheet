package tn.esprit.timesheet.reposetoris;

import org.springframework.data.repository.CrudRepository;
import tn.esprit.timesheet.entities.Timesheet;

public interface ITimesheetRepository extends CrudRepository<Timesheet,Long> {
}
