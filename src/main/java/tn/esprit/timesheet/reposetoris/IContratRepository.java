package tn.esprit.timesheet.reposetoris;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.timesheet.entities.Contrat;
@Repository
public interface IContratRepository extends CrudRepository<Contrat, Long> {
}
